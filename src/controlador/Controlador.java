package controlador;
// Importa el modelo y vista

import java.awt.event.ActionEvent;
import modelo.Fecha;
import vista.vdlgFecha;
import java.awt.event.ActionListener;

// Importar clase e interface que se implementaran
// para escuchar a la vista
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Controlador implements ActionListener {

    private Fecha hoy;
    private vdlgFecha vista;

    public Controlador(Fecha hoy, vdlgFecha vista) {
        this.hoy = hoy;
        this.vista = vista;
        // Hace que el controlador escuche los eventos de los botones
        // que estan en la vista.
        vista.btnMostrar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
    }

    private void iniciarVista() {
        vista.setTitle("Fecha ");
        vista.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.btnGuardar) {
            hoy.setDia(Integer.parseInt(vista.txtDia.getText()));
            hoy.setMes(Integer.parseInt(vista.txtMes.getText()));
            hoy.setAño(Integer.parseInt(vista.txtAnio.getText()));
        }

        if (e.getSource() == vista.btnMostrar) {
            vista.txtDia.setText(String.valueOf(hoy.getDia()));
            vista.txtMes.setText(String.valueOf(hoy.getMes()));
            vista.txtAnio.setText(String.valueOf(hoy.getAño()));
            if (hoy.isBisiesto()) {
                JOptionPane.showMessageDialog(vista, "Es una año bisiesto");
            }
        }

        if (e.getSource() == vista.btnLimpiar) {
            vista.txtDia.setText("");
            vista.txtMes.setText("");
            vista.txtAnio.setText("");
        }
    }

    public static void main(String[] args) {
        
        Fecha hoy = new Fecha();
        vdlgFecha vista = new vdlgFecha(new JFrame(), true);
        
        Controlador control = new Controlador(hoy, vista);
        control.iniciarVista();
    }

}
